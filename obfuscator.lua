local token = "INSERTRANDOMLETTERSORSMARTPHRASE"


local scrInstance = workspace.ToObfuscate -- Choose script from workspace
local strToByte = string.byte
local text = ""                                  -- Or use it directly


local generateNameFromToken = function(len)
	local gen = ""
	for i = 1, len do
		local z = math.random(1, string.len(token))
		local IRZZZIAMMMMIBBBNGGGIDIOIM = string.sub(token, z, z)
		gen = gen..IRZZZIAMMMMIBBBNGGGIDIOIM
	end
	return gen
end

local loadstringname = generateNameFromToken(75)
local names = {
		generateNameFromToken(32),
		generateNameFromToken(32),
		generateNameFromToken(32),
		generateNameFromToken(32),
		generateNameFromToken(32),
		generateNameFromToken(32),
		generateNameFromToken(32),
		generateNameFromToken(32),
		generateNameFromToken(32)
}



function Obfuscate(source)
	local scriptInBytesToString = "local "..names[9].. " = { "
	local scriptInBytes = {}
	
	for i = 1, string.len(source) do
		table.insert( scriptInBytes , i , strToByte( string.sub( source , i , i ) ) )
	end
	for i, v in pairs(scriptInBytes) do
		if i < source:len() then
			scriptInBytesToString = scriptInBytesToString.." '\\"..v.."', "
		else
			scriptInBytesToString = scriptInBytesToString.." '\\"..v.."' }"
		end
	end
	return scriptInBytesToString
end
local methods = {
	 MethodInstance = function()
		local Obfuscated = "-- FriendlyObfuscator by FriendlyGuy\nlocal "..names[1].." = 'DONT MAKE ME CRY, DONT, PLEASE, DONT ANALYZE IT OR I WILL CRY (c) FriendlyGuy'; local "..names[3].." = 'WHYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYY'; local "..names[4].." = "..names[1]..".."..names[3].."; local "..names[5].." = ".."'STOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOP AAAAAAAAAAAAAAAAANNNNNNNNNAAAAAAAAAAAAAAAAAAAAAAALYYYYYYYYYYYYYYZE'.."..names[4].."; local "..loadstringname.." = loadstring; "..Obfuscate(scrInstance.Source).."; local "..names[8].." = ''; for i, v in pairs("..names[9]..") do "..names[8].." = "..names[8]..".."..names[9].."[i] end; "..loadstringname.."("..names[8]..")()"
		local ScriptToOutput = Instance.new("Script")
		ScriptToOutput.Source = Obfuscated
		ScriptToOutput.Name = "FriendlyObfuscator Obfuscated Script"
		ScriptToOutput.Parent = workspace
	end,
     MethodText = function( )
		local Obfuscated = "-- FriendlyObfuscator by FriendlyGuy\nlocal "..names[1].." = 'DONT MAKE ME CRY, DONT, PLEASE, DONT ANALYZE IT OR I WILL CRY (c) FriendlyGuy'; local "..names[3].." = 'WHYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYY'; local "..names[4].." = "..names[1]..".."..names[3].."; local "..names[5].." = ".."'STOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOP AAAAAAAAAAAAAAAAANNNNNNNNNAAAAAAAAAAAAAAAAAAAAAAALYYYYYYYYYYYYYYZE'.."..names[4].."; local "..loadstringname.." = loadstring; "..Obfuscate(text).."; local "..names[8].." = ''; for i, v in pairs("..names[9]..") do "..names[8].." = "..names[8]..".."..names[9].."[i] end; "..loadstringname.."("..names[8]..")()"
		local ScriptToOutput = Instance.new("Script")
		ScriptToOutput.Source = Obfuscated
		ScriptToOutput.Name = "FriendlyObfuscator Obfuscated Script"
		ScriptToOutput.Parent = workspace
	end
	}
if scrInstance ~= nil and text ~= "" then
	error("You can't use text and instance method at same time! :<")
elseif scrInstance ~= nil and text == "" then
	methods.MethodInstance()
elseif scrInstance == nil and text ~= "" then
	methods.MethodText()
else
	error("You should set your script instance or text source of your script")
end

print("Done")